import React from 'react';
import {View, Image, StyleSheet, Text, ImageBackground} from 'react-native';

const Header = () => {
  return (
    <View style={styles.view}>
      <View style={styles.topBar}>
        <View style={styles.userContainer}>
          <Image
            style={styles.user}
            source={require('../assets/images/user.jpg')}
          />
        </View>
        <Image
          style={styles.logo}
          source={require('../assets/images/reload.png')}
        />
        <Image source={require('../assets/images/chat_icon.png')} />
      </View>
      <View
        style={{
          ...styles.bannerContainer,
        }}>
        <ImageBackground
          style={styles.banner}
          source={require('../assets/images/ilustration_lifestyle.png')}>
          <View style={styles.textContainer}>
            <Text style={styles.highlight}>Lifestyle</Text>
            <Text style={styles.text}>
              Get a holistic view of your activities to enhance your wellbeing
              and benefit from even more accurate recommendations.
            </Text>
          </View>
        </ImageBackground>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
  },
  user: {
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: 100,
    padding: 10,
  },
  userContainer: {
    shadowColor: 'black',
    shadowRadius: 10,
    shadowOpacity: 0.6,
    elevation: 10,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    overflow: 'hidden',
    borderRadius: 100,
    maxWidth: 50,
    aspectRatio: 1,
    flex: 1,
    backgroundColor: 'white',
    padding: 3,
  },
  logo: {
    maxWidth: 130,
    aspectRatio: 5,
  },
  topBar: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bannerContainer: {
    aspectRatio: 2.4,
  },
  banner: {
    width: '100%',
    height: '100%',
  },
  text: {
    color: 'white',
    fontFamily: 'Roboto',
  },
  highlight: {
    marginVertical: 10,
    color: 'white',
    fontWeight: '700',
    fontFamily: 'Roboto',
  },
  textContainer: {
    paddingLeft: 10,
    width: '50%',
  },
});

export default Header;
