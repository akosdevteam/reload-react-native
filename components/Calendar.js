import {
  addMonths,
  eachDayOfInterval,
  endOfMonth,
  format,
  getDay,
  getWeeksInMonth,
  isBefore,
  isWithinInterval,
  startOfMonth,
} from 'date-fns';
import LinearGradient from 'react-native-linear-gradient';
import {isNull} from 'lodash';
import React, {useCallback, useEffect, useState} from 'react';
import {Modal, Pressable, StyleSheet, Text, View} from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Calendar = ({visible, onClose}) => {
  const [date, setDate] = useState(new Date());
  const [calendarTitle, setCalendarTitle] = useState('');
  const [weekDates, setWeekDates] = useState([]);
  const [selectedDate, setSelectedDate] = useState({
    start: null,
    end: null,
  });
  const weekDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

  const isWeekDayActive = weekDay =>
    !isNull(selectedDate.start) &&
    format(weekDay, 'ddMMyyyy') === format(selectedDate.start, 'ddMMyyyy');

  const isWeekDayInRange = weekDay =>
    !isNull(selectedDate.end) &&
    isWithinInterval(weekDay, selectedDate) &&
    format(weekDay, 'ddMMyyyy') !== format(selectedDate.start, 'ddMMyyyy');

  const selectDate = changedSelectedDate => {
    if (selectedDate.start !== null && selectedDate.end !== null) {
      setSelectedDate({
        start: changedSelectedDate,
        end: null,
      });
    } else if (selectedDate.start === null) {
      setSelectedDate({
        start: changedSelectedDate,
        end: null,
      });
    } else {
      if (!isBefore(selectedDate.start, changedSelectedDate)) {
        setSelectedDate({
          start: changedSelectedDate,
          end: selectedDate.start,
        });
        return;
      }

      setSelectedDate({
        start: selectedDate.start,
        end: changedSelectedDate,
      });
    }
  };

  const mountCalendarStructure = changedDate => {
    const calendarDays = eachDayOfInterval({
      start: startOfMonth(changedDate),
      end: endOfMonth(changedDate),
    });

    const indexOfFirstDay = getDay(calendarDays[0]);
    const weeks = Array(getWeeksInMonth(changedDate)).fill([]);
    const parsedCalendarDays = weeks.map((weekDate, index) => {
      if (index === 0) {
        weekDate = Array(7).fill(null);
        for (var i = indexOfFirstDay; i <= 6; i++) {
          weekDate[i] = calendarDays.splice(0, 1)[0];
        }
        return weekDate;
      }
      return calendarDays.splice(0, 7);
    });

    const fillLastSquaresCount =
      7 - parsedCalendarDays[parsedCalendarDays.length - 1].length;

    parsedCalendarDays[parsedCalendarDays.length - 1] = parsedCalendarDays[
      parsedCalendarDays.length - 1
    ].concat(Array(fillLastSquaresCount).fill(null));

    return parsedCalendarDays;
  };

  const changeDate = inc => {
    setDate(addMonths(date, inc));
    setWeekDates(mountCalendarStructure(date));
  };

  const updateCalendar = useCallback(changedDate => {
    setCalendarTitle(format(changedDate, 'cccc - LLL dd'));
    setWeekDates(mountCalendarStructure(changedDate));
  }, []);

  const closeModal = () => onClose();

  useEffect(() => {
    updateCalendar(date);
  }, [date, updateCalendar]);

  return (
    <GestureRecognizer onSwipeDown={closeModal}>
      <Modal visible={visible} animationType="slide" transparent={true}>
        <View style={styles.overlay}>
          <View style={styles.modal}>
            <View style={styles.titleContent}>
              <Pressable onPress={closeModal}>
                <Icon style={styles.iconTitle} name="close" size={20} />
              </Pressable>
              <Text style={styles.title}>Calendar</Text>
            </View>
            <View style={styles.header}>
              <Pressable onPress={() => changeDate(-1)}>
                <Icon style={styles.headerIcon} name="arrow-back" size={20} />
              </Pressable>
              <Text style={styles.headerText}>{calendarTitle}</Text>
              <Pressable onPress={() => changeDate(1)}>
                <Icon
                  style={styles.headerIcon}
                  name="arrow-forward"
                  size={20}
                />
              </Pressable>
            </View>
            <View style={styles.divider} />
            <View style={styles.weeksContainer}>
              <LinearGradient
                colors={['rgba(74, 84, 223, 0.3)', 'rgba(21, 212, 216, 0.2)']}
                start={{x: 0.9, y: 0.3}}>
                <View style={styles.week}>
                  {weekDays.map((weekDay, index) => (
                    <View style={styles.weekDayContainer} key={index}>
                      <Text style={[styles.weekDay, styles.weekDayTitle]}>
                        {weekDay}
                      </Text>
                    </View>
                  ))}
                </View>
                {weekDates.map((weekDate, parentIndex) => (
                  <View style={styles.week} key={parentIndex}>
                    {weekDate.map((weekDay, index) => (
                      <View style={styles.weekDayContainer} key={index}>
                        {isNull(weekDay) ? (
                          <Pressable style={styles.pressable}>
                            <View style={styles.padding} />

                            <Text style={styles.weekDay} />

                            <View style={styles.padding} />
                          </Pressable>
                        ) : (
                          <Pressable
                            style={styles.pressable}
                            onPress={() => selectDate(weekDay)}>
                            <View style={styles.padding} />

                            <Text
                              style={[
                                styles.weekDay,
                                isWeekDayActive(weekDay)
                                  ? styles.weekDayActive
                                  : {},
                                isWeekDayInRange(weekDay)
                                  ? styles.weekDayRange
                                  : {},
                              ]}>
                              {format(weekDay, 'dd')}
                            </Text>

                            <View style={styles.padding} />
                          </Pressable>
                        )}
                      </View>
                    ))}
                  </View>
                ))}
              </LinearGradient>
            </View>
            <Pressable>
              <LinearGradient
                style={styles.button}
                colors={['#4a54df', '#15d4d8']}
                start={{x: 0, y: 1}}>
                <Text style={styles.buttonText}>Apply</Text>
              </LinearGradient>
            </Pressable>
          </View>
        </View>
      </Modal>
    </GestureRecognizer>
  );
};

const defaultWeekDay = {
  flexDirection: 'row',
  textAlign: 'center',
  color: 'black',
  fontSize: 10,
  paddingVertical: 8,
};

const styles = StyleSheet.create({
  pressable: {
    flexDirection: 'column',
    maxHeight: 40,
  },
  padding: {
    backgroundColor: 'white',
    width: '100%',
    paddingBottom: 10,
  },
  overlay: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
    flexDirection: 'column-reverse',
  },
  modal: {
    backgroundColor: 'white',
    height: '60%',
    minHeight: 450,
    padding: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  titleContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontWeight: '700',
    lineHeight: 30,
    letterSpacing: 2,
    fontSize: 12,
  },
  iconTitle: {
    marginRight: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    paddingVertical: 15,
    fontWeight: '700',
    color: '#999999',
    letterSpacing: 2,
    fontSize: 12,
  },
  headerIcon: {
    paddingVertical: 15,
    fontWeight: '700',
    color: '#BBBBBB',
  },
  divider: {
    backgroundColor: '#CCCCCC',
    width: '100%',
    height: 2,
  },
  weeksContainer: {
    flex: 1,
  },
  week: {
    flexDirection: 'row',
  },
  weekDayContainer: {
    flexBasis: '14.2857%',
  },
  weekDay: {
    ...defaultWeekDay,
    backgroundColor: 'white',
  },
  weekDayTitle: {
    fontWeight: '700',
    textAlign: 'center',
  },
  weekDayActive: {
    ...defaultWeekDay,
    color: 'white',
    borderColor: 'black',
    backgroundColor: '#15d4d8',
  },
  weekDayRange: {
    backgroundColor: 'transparent',
    color: 'black',
    borderRadius: 100,
  },
  button: {
    width: '85%',
    marginLeft: '7.5%',
    marginTop: 10,
    padding: 15,
    borderRadius: 50,
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
});

export default Calendar;
