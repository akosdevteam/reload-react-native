import React, {useState} from 'react';
import {
  Button,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Calendar from './components/Calendar';
import Header from './components/Header';

const App = () => {
  const [visible, setVisible] = useState(true);
  const [feedLabelActive, setFeedLabelActive] = useState('activity');

  const changeActivedFeedLabel = actived => setFeedLabelActive(actived);
  const openCalendar = () => {
    setVisible(true);
  };

  return (
    <ScrollView>
      <Header />
      <View style={styles.feedControls}>
        <Pressable onPress={() => changeActivedFeedLabel('timestamp')}>
          <Text
            style={[
              styles.feedLabels,
              feedLabelActive === 'timestamp' ? styles.activeFeed : {},
            ]}>
            Timestamp feed
          </Text>
        </Pressable>
        <Pressable onPress={() => changeActivedFeedLabel('activity')}>
          <Text
            style={[
              styles.feedLabels,
              feedLabelActive === 'activity' ? styles.activeFeed : {},
            ]}>
            Activity feed
          </Text>
        </Pressable>
      </View>
      <Calendar visible={visible} onClose={() => setVisible(false)} />
      <Button onPress={() => openCalendar()} title="Select date..." />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  feedControls: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  feedLabels: {
    fontFamily: 'Roboto',
    padding: 10,
  },
  activeFeed: {
    fontWeight: '700',
  },
});

export default App;
